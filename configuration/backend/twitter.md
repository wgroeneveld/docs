### Twitter OAuth Configuration

<div class="setting-right">Optional</div>
<div class="setting-title">COMMENTO_TWITTER_KEY</div>
<div class="setting-title">COMMENTO_TWITTER_SECRET</div>

Twitter OAuth configuration. Create an app in the [Twitter developer dashboard](https://developer.twitter.com/en/apps) to generate a set of credentials. By default, Twitter login is turned off when these values are left empty.

<div class="setting-example">
COMMENTO_TWITTER_KEY=HsWM4q8lcKNiv6idWvRdeSjS<br>
COMMENTO_TWITTER_SECRET=9j60WfN3LG6GAMbU5LCch1HQ6tT4ytiOzO95rM3DVD5dXHFT
</div>

--- 

### Twitter App configuration

The following App Details should be filled in using the the [Twitter developer dashboard](https://developer.twitter.com/en/apps):

1. Sign in with Twitter: `enabled`
2. Callback URL: `https://commento.example.com/api/oauth/twitter/callback`. 
3. Terms of Service URL: a valid URL pointing to your domain.
4. Privacy Policy URL: a valid URL pointing to your domain.
5. Organization website URL: `https://example.com`

HTTPS is required for the Twitter callback URL to work.

The following Permissions should be granted to the app:

1. Access permission: `Read-only`
2. Additional permissions: `Request email address`

<p style="text-align: center"><img src="twitter.png"></img></p>

After that, you should be able to login with your Twitter credentials.
